#!/usr/bin/python
# -*- coding: utf8 -*-
import os
if not os.path.isdir("letsencrypt"):
    os.system("sudo a2enmod ssl")
    os.system("sudo a2ensite default-ssl.conf")
    os.system("sudo systemctl restart apache2.service")
    os.system("sudo apt-get -y install git")
    os.system(" git clone https://github.com/letsencrypt/letsencrypt")
    os.system("./letsencrypt/letsencrypt-auto")
else:
    os.system("./letsencrypt/letsencrypt-auto")